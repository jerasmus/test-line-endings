// Run this script via `node test.js` to print out the contents of the file (including line endings)
const fs = require("fs");
const fileContents = fs.readFileSync("./CRLF").toString();
console.log(JSON.stringify(fileContents));
